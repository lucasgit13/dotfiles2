(setq user-full-name "Lucas Souza"
      user-mail-address "lucas.araujo.souza13@gmail.com")

(map! :n "<f5>" 'evil-write
      :n "<f10>" 'evil-save-and-quit)

(map! :leader :desc "M-x" :g "SPC" 'counsel-M-x)

(map! :leader :desc "Eshell" :n "e e" 'eshell)
(map! :leader :desc "Eshell command" :n "e c" 'eshell-command)

(map! :leader :map rust-mode-map "c c" 'rust-run)



(setq doom-font (font-spec :family "Mononoki Nerd Font" :size 18)
      doom-variable-pitch-font (font-spec :family "Hack" :size 16)
      doom-big-font (font-spec :family "JoyPixels" :size 24))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))
(setq global-prettify-symbols-mode t)
    (add-to-list 'default-frame-alist '(font . "Mononoki Nerd Font-18"))

(setq doom-theme 'doom-one)

(setq org-directory "~/org/")

(with-eval-after-load 'org
;; This is needed as of Org 9.2
(use-package org-tempo)

(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("py" . "src python")))

(setq doom-font (font-spec :family "Mononoki Nerd Font" :size 20)
      doom-variable-pitch-font (font-spec :family "Hack") ; inherits `doom-font''s :size
      doom-unicode-font (font-spec :family "JoyPixels" :size 14)
      doom-big-font (font-spec :family "Hack" :size 19))

(setq display-line-numbers-type t)
