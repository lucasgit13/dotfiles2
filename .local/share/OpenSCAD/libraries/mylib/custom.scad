module heart_points(n,h){
function heart_coordinates(t) = [16*pow(sin(t),3), 13*cos(t) - 5*cos(2*t) - 2*cos(3*t) - cos(4*t)];
function heart_points(n) = [ for (t=[0:360/n:359.999]) heart_coordinates(t)];
points = heart_points(n=n);
linear_extrude(height=h)
    polygon(points);
}
//--
module circle_edge(r=5,w=2,l=20,hole=false,r2,$fn=30){
    d=l/2;
    if(hole==false){
        union(){
            cylinder(h=w,r=r,center=true,$fn=$fn);
            translate([d,0,0]) cube([l,r*2,w],center=true);
               }
    }

     if(hole==true){
difference(){     
        union(){
            cylinder(h=w,r=r,center=true,$fn=$fn);
            translate([d,0,0]) cube([l,r*2,w],center=true);
               }
        translate([0,0,-0.5]) cylinder(h=w+1,r=r2,$fn=$fn);
            }
    }
    
}
//--
module chamf(r=1){
    sq=r*2;
    sq2=(r*2)+1;
    difference(){
        square(sq,center=true);
        circle(r=r,$fn=60);
        
        translate([0,-sq2/2,0]) square(sq2,center=true);
        translate([-sq2/2,sq2/2,0]) square(sq2,center=true);
    }
}